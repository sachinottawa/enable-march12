import { useEffect, useState } from 'react'
import './App.css'

function App() {
  const [temperature, setTemperature] = useState(0)
  const [city, setCity] = useState('Kochi')

  useEffect(() => {
    let longitude
    let latitude

    if (city === 'Kochi') {
      latitude = 9.9312
      longitude = 76.2673
    }
    else if (city === 'Trivandrum') {
      latitude = 8.5241
      longitude = 76.9366
    }
    else {
      latitude = 9.4981
      longitude = 76.3388
    }

    fetch(`https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`)
      .then(response => response.json())
      .then(data => setTemperature(data.current.temperature_2m))
      .catch(error => console.log(error))
  }, [city])

  return (
    <>
      <header></header>
      <main>
        <h1>Temperature at {city} is <span id='temperature'>{temperature}&deg;C</span></h1>
        <button onClick={() => setCity('Kochi')}>Kochi</button>
        <button onClick={() => setCity('Trivandrum')}>Trivandrum</button>
        <button onClick={() => setCity('Alappuzha')}>Alappuzha</button>
      </main>
      <footer></footer>
    </>
  )
}

export default App
